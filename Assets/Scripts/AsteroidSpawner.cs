﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class AsteroidSpawner : MonoBehaviour
{
    [SerializeField] private GameObject AsteriodPrefab;
    [SerializeField] private BallWaves Waves;

    private GameObject Earth;
    private float WaveStartTime;

    private int WaveIndex;
    
    // Start is called before the first frame update
    private void Start()
    {
        Earth = GameObject.FindWithTag("Earth");
        WaveIndex = 0;
        WaveStartTime = Time.time;
    }

    // Update is called once per frame
    private void Update()
    {
        var curWaveTime = Time.time - WaveStartTime;
        var curWave = Waves.Waves[WaveIndex];
        var waveStatus = GetCurWaveStatus(curWaveTime, curWave);
        
        // check wave change
        switch (waveStatus)
        {
            case WaveStatus.Preparing:
                WavePreparing();
                break;
            case WaveStatus.Running:
                WaveRunning(curWaveTime, curWave);
                break;
            case WaveStatus.Sleeping:
                WaveSleep();
                break;
            case WaveStatus.Done:
                WaveIndex++;
                WaveStartTime = Time.time;
                if (WaveIndex >= Waves.Waves.Count)
                {
                    // TODO - Also could be GameOver
                    WaveIndex = 0;
                }
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        
    }

    // TODO - Animation or something
    private void WavePreparing()
    {
        
    }

    private void WaveRunning(float curTime, BallWave wave)
    {
        var a = Random.Range(0.0f, 1.0f);
        Debug.Log(a + "_" + wave.Posibility.Evaluate(curTime));
        if (a < wave.Posibility.Evaluate(curTime))
        {
            var asteroid = Instantiate(AsteriodPrefab);

            // Set init position
            var angle = Random.Range(0.0f, 360.0f);
            var q = Quaternion.AngleAxis(angle, Vector3.forward);
            
            asteroid.transform.position = Earth.transform.position + q * Vector3.right * 20;
            
            // Calc and set velocity
            var averageVelocity = wave.AverageVelocity.Evaluate(curTime);
            var velocity = Random.Range(averageVelocity - wave.VelocityOffset, averageVelocity + wave.VelocityOffset);
            var mover = asteroid.GetComponent<SolarBallMover>();
            
            // Calc direction
            var earthTargetDir = (Earth.transform.position - asteroid.transform.position).normalized;
            mover.Setup(velocity, earthTargetDir);
        }
    }

    // TODO - Animation or something
    private void WaveSleep()
    {
        
    }

    private WaveStatus GetCurWaveStatus(float curWaveTime, BallWave wave)
    {
        if (curWaveTime < wave.PrepareTime)
            return WaveStatus.Preparing;
        if (curWaveTime < wave.PrepareTime + wave.RunningTime)
            return WaveStatus.Running;
        if (curWaveTime < wave.PrepareTime + wave.RunningTime + wave.SleepTime)
            return WaveStatus.Sleeping;
        return WaveStatus.Done;
    }
    
    private float GetNumberInNormalDistribution(float mean,float dev)
    {
        return mean + (RandomNormalDistribution() * dev);
    }

    private float RandomNormalDistribution()
    {
        var u = 0.0f;
        var w = 0.0f;
        while (w == 0.0 || w >= 1.0)
        {
            u = Random.Range(-1, 1) * 2 - 1.0f;
            var v = Random.Range(-1, 1) * 2 - 1.0f;
            w = u * u + v * v;
        }

        var c = Math.Sqrt((-2 * Math.Log(w)) / w);
        return u * (float) c;
    }
}
