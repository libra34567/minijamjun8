﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class SolarBallSpawner : MonoBehaviour
{
    [SerializeField] private GameObject SunLightPrefab;
    [SerializeField] private GameObject SunFlamePrefab;
    [SerializeField] private BallWaves Waves;

    private GameObject Earth;
    private float WaveStartTime;

    [SerializeField] private int WaveIndex;
    
    // Start is called before the first frame update
    private void Start()
    {
        Earth = GameObject.FindWithTag("Earth");
        WaveIndex = 0;
        WaveStartTime = Time.time;
    }

    // Update is called once per frame
    private void Update()
    {
        var curWaveTime = Time.time - WaveStartTime;
        var curWave = Waves.Waves[WaveIndex];
        var waveStatus = GetCurWaveStatus(curWaveTime, curWave);
        
        // check wave change
        switch (waveStatus)
        {
            case WaveStatus.Preparing:
                WavePreparing();
                break;
            case WaveStatus.Running:
                WaveRunning(curWaveTime, curWave);
                break;
            case WaveStatus.Sleeping:
                WaveSleep();
                break;
            case WaveStatus.Done:
                WaveIndex++;
                WaveStartTime = Time.time;
                if (WaveIndex >= Waves.Waves.Count)
                {
                    // TODO - Also could be GameOver
                    WaveIndex = 0;
                }
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        
    }

    // TODO - Animation or something
    private void WavePreparing()
    {
        
    }

    private void WaveRunning(float curTime, BallWave wave)
    {
        if (Random.Range(0f, 1f) < wave.Posibility.Evaluate(curTime))
        {
            var sunBall = wave.Type == BallType.Sunlight ? Instantiate(SunLightPrefab) : Instantiate(SunFlamePrefab);
            
            // Set init position
            var solarBallOffsetX = Random.Range(-wave.SunWidth, wave.SunWidth);
            var solarBallOffsetY = Random.Range(-wave.SunWidth, wave.SunWidth);
            sunBall.transform.position = transform.position + new Vector3(solarBallOffsetX, solarBallOffsetY, 0);
            
            // Calc and set velocity
            var averageVelocity = wave.AverageVelocity.Evaluate(curTime);
            var velocity = Random.Range(averageVelocity - wave.VelocityOffset, averageVelocity + wave.VelocityOffset);
            var mover = sunBall.GetComponent<SolarBallMover>();
            
            // Calc direction
            var earthTargetDir = (Earth.transform.position - transform.position).normalized;
            mover.Setup(velocity, earthTargetDir);
        }
    }

    // TODO - Animation or something
    private void WaveSleep()
    {
        
    }

    private WaveStatus GetCurWaveStatus(float curWaveTime, BallWave wave)
    {
        if (curWaveTime < wave.PrepareTime)
            return WaveStatus.Preparing;
        if (curWaveTime < wave.PrepareTime + wave.RunningTime)
            return WaveStatus.Running;
        if (curWaveTime < wave.PrepareTime + wave.RunningTime + wave.SleepTime)
            return WaveStatus.Sleeping;
        return WaveStatus.Done;
    }
    
    // Normal Distribution Random, if we need
    private float GetNumberInNormalDistribution(float mean,float dev)
    {
        return mean + (RandomNormalDistribution() * dev);
    }

    private float RandomNormalDistribution()
    {
        var u = 0.0f;
        var w = 0.0f;
        while (w == 0.0 || w >= 1.0)
        {
            u = Random.Range(-1, 1) * 2 - 1.0f;
            var v = Random.Range(-1, 1) * 2 - 1.0f;
            w = u * u + v * v;
        }

        var c = Math.Sqrt((-2 * Math.Log(w)) / w);
        return u * (float) c;
    }
}

public enum WaveStatus
{
    None = 0,
    Preparing = 1,
    Running = 2,
    Sleeping = 3,
    Done = 4,
}
