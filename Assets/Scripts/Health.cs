using UnityEngine;

public class Health : MonoBehaviour
{
    public float health;

    public virtual void ApplyDamage(float damage)
    {
        health -= damage;
        if(health <= 0)
        {
            Destroy(gameObject);
        }
    }




}
