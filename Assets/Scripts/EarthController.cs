﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;


public class EarthController : MonoBehaviour
{
    public float placementDegree;
    public GameObject panelPlacer;
    public float placementRotationSpeed;
    public float rotationSpeed;
    public GameObject solarPanelPrefab;
    public GameObject shieldPrefab;
    public PanelController placementPanelController;
    public DataKeeper dataKeeper;

    private bool inCreationMode;
    private Player _controller;

    public List<GameObject> solarPanels = new List<GameObject>();
    public List<float> individualDegrees;

    public float degreeOffset;

    public float radius;

    public float solarPanelCost;
    public float shieldCost;

    // Start is called before the first frame update
    void Start()
    {
        _controller = ReInput.players.GetPlayer(0);
        placementDegree = 0;
        panelPlacer.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (!inCreationMode && _controller.GetButtonDown("ModeSwitch"))
        {
            var position = degreeToPosition(0);
            panelPlacer.transform.position = position;
            placementDegree = -degreeOffset;
            panelPlacer.SetActive(true);
            inCreationMode = true;
        }else if(inCreationMode && _controller.GetButtonDown("ModeSwitch"))
        {
            panelPlacer.SetActive(false);
            inCreationMode = false;
        }
        if(inCreationMode)
        {
            //no need to double check it just happened
            if (_controller.GetButton("RotationL"))
            {
                var direction = 1;
                placementDegree += direction * placementRotationSpeed * Time.deltaTime;
            }
            if (_controller.GetButton("RotationR"))
            {
                var direction = -1;
                placementDegree += direction * placementRotationSpeed * Time.deltaTime;
            }

            if (_controller.GetButtonDown("BuildSolar"))
            {
                GeneratePanel(solarPanelPrefab);
            }
            if (_controller.GetButtonDown("BuildShield"))
            {
                GeneratePanel(shieldPrefab);
            }
        }

        if (!inCreationMode)
        {
            var direction = 0;
            if (_controller.GetButton("RotationL"))
            {
                direction = 1;
            }
            if (_controller.GetButton("RotationR"))
            {
                direction = -1;
            }
            degreeOffset += direction * rotationSpeed * Time.deltaTime;
        }
    }

    public void FixedUpdate()
    {
        UpdateAliveGameobjects();
        /**
         * Every frame we use the global offset degree and the individual degree to get the degree of every panel
         * with that degree we calculate the current position of that panel (sin/cos) and set the transform
         */
        for (int i = 0; i < solarPanels.Count; i++)
        {
            var solarPanel = solarPanels[i];
            var individualDegree = individualDegrees[i];
            var degree = degreeOffset + individualDegree;
            solarPanel.transform.position = degreeToPosition(degree);
        }
    }

    public Vector3 degreeToPosition(float degree)
    {
        var x = Mathf.Sin(degree) * radius;
        var y = Mathf.Cos(degree) * radius;
        return new Vector3(x, y, 0);
    }

    private void UpdateAliveGameobjects()
    {
        for(int i = solarPanels.Count - 1; i >= 0; i--)
        {
            if (solarPanels[i] == null)
            {
                solarPanels.RemoveAt(i);
                individualDegrees.RemoveAt(i);
            }
        }
    }

    private void GeneratePanel(GameObject prefab)
    {
        var cost = 0f;
        if(prefab == solarPanelPrefab)
        {
            Debug.Log("Trying to build solar panel");
            cost = solarPanelCost;
        }
        if(prefab == shieldPrefab)
        {
            Debug.Log("Trying to build shield");
            cost = shieldCost;
        }

        if(dataKeeper.energy >= cost)
        {
            if (placementPanelController.placementIsValid)
            {
                dataKeeper.AddEnergy(-cost);
                var degree = degreeOffset + placementDegree;
                individualDegrees.Add(placementDegree);
                var position = degreeToPosition(degree);
                var newPanel = Instantiate(prefab, position, Quaternion.identity);
                newPanel.transform.SetParent(transform);
                solarPanels.Add(newPanel);
            }
            else
            {
                Debug.Log("Sorry but honestly you knew you couldn't place another panel there.");
            }
        }
        else
        {
            Debug.Log("You don't have enough energy anymore");
        }
    }
}