using System;
using UnityEngine;

public class BadSolarBall : MonoBehaviour
{
    [SerializeField] private float _damage;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("VisibleArea")) return;
        
        if (other.CompareTag("SolarPanel") || other.CompareTag("Earth") || other.CompareTag("Human"))
        {
            var health = other.GetComponent<Health>();
            if (health)
            {
                health.ApplyDamage(_damage);
            }
        }
        DestroySolarBall();
    }

    private void DestroySolarBall()
    {
        // TODO - Play Animation
        Destroy(gameObject);
    }
}
