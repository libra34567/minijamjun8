﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelController : MonoBehaviour
{
    public EarthController earthController;

    public bool placementIsValid = true;

    public float timerCooldown = 0.01f;

    private float timer;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        var degree = earthController.degreeOffset + earthController.placementDegree;
        var radius = earthController.radius;
        var position = earthController.degreeToPosition(degree);
        transform.position = position;

        timer -= Time.deltaTime;
        if(timer <= 0)
        {
            timer = 0;
            placementIsValid = true;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.CompareTag("Shield") || other.CompareTag("SolarPanel"))
        {
            placementIsValid = false;
            timer = timerCooldown;
        }
    }
}
