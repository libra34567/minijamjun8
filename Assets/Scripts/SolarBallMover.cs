﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolarBallMover : MonoBehaviour
{
    private float _speed;
    private Vector3 _dirV3;

    private bool isSetup = false;

    public void Setup(float speed, Vector3 dirV3)
    {
        _speed = speed;
        _dirV3 = dirV3;
        isSetup = true;
    }


    public void Update()
    {
        if (!isSetup)
            return;
        
        transform.position += _dirV3.normalized * _speed * Time.deltaTime;
    }

}
