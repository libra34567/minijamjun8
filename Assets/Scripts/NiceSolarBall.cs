using System;
using UnityEngine;

public class NiceSolarBall : MonoBehaviour
{
    [SerializeField] private float _energy;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("VisibleArea")) return;
        
        if (other.CompareTag("SolarPanel"))
        {
            var energyReceiver = other.GetComponent<EnergyReceiver>();
            if (energyReceiver)
            {
                energyReceiver.ApplyEnergy(_energy);
            }
        }
        DestroySolarBall();
    }

    private void DestroySolarBall()
    {
        // TODO - Play Animation
        Destroy(gameObject);
    }
}