﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour
{

    public int populationPerSpawnpoint = 0;
    public TextMeshProUGUI energyText;
    public TextMeshProUGUI gameOverText;
    public Text populationText;
    private int currentPopulation;
    

    private float energy;
    // Start is called before the first frame update
    void Start()
    {
        if (energyText == null)
        {
            Debug.LogError("No Energy Text provided to Score Controller!");
        }
        Debug.Log($"EnergyLevel is {energy}");
        SetEnergy(energy);
        
        if (gameOverText == null)
        {
            Debug.LogError("No Game Over Text provided to Score Controller!");
        }
        gameOverText.SetText("");

        currentPopulation = populationPerSpawnpoint *
                            GameObject.Find("Earth").GetComponent<HumanSpawner>().SpawnPoints.Count;
        UpdatePopulation(0);
    }

    public void SetEnergy(float newValue)
    {
        energy = newValue;
        energyText.SetText($"Energy: {energy}");

        if (energy < 0)
        {
            GameOver();
        }
    }

    public void UpdatePopulation(int decrement)
    {
        currentPopulation -= decrement * populationPerSpawnpoint;
        populationText.text = "Total Population: " + currentPopulation;
        
        if (currentPopulation <= 0)
        {
            GameOver();
        }
    }

    public float GetEnergy()
    {
        return energy;
    }

    void GameOver()
    {
        Debug.Log("GameOver");
        gameOverText.SetText("Game Over!");
    }
    
    
}
