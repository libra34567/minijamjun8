﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataKeeper : MonoBehaviour
{

    public float energy = 1000;

    public ScoreController scoreController;
    // Start is called before the first frame update
    void Start()
    {
        scoreController.SetEnergy(energy);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddEnergy(float toAdd)
    {
        energy += toAdd;
        scoreController.SetEnergy(energy);
        Debug.Log("Energy at " + energy);
    }
}
