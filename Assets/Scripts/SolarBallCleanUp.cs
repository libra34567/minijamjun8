
using System;
using UnityEngine;

public class SolarBallCleanUp : MonoBehaviour
{
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("VisibleArea"))
        {
            Destroy(gameObject);
        }
    }
}
