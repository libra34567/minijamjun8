using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Wave", menuName = "Scriptable/SolarWaves", order = 1)]
public class BallWaves : ScriptableObject
{
    public List<BallWave> Waves;
}

public enum BallType
{
    None = 0,
    Sunlight = 1,
    SunFlame = 2, 
    Asteroid = 3
}

[Serializable]
public class BallWave
{
    public float PrepareTime;
    public BallType Type;
    public AnimationCurve Damage;
    public AnimationCurve AverageVelocity;
    public float VelocityOffset;
    public AnimationCurve Posibility;
    public float SunWidth;
    public float DirectionOffset;
    public float RunningTime;
    public float SleepTime;
}
