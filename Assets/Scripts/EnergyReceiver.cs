
using UnityEngine;

public class EnergyReceiver : MonoBehaviour
{

    public void ApplyEnergy(float energy)
    {
        DataKeeper dataKeeper = (DataKeeper) FindObjectOfType<DataKeeper>();
        dataKeeper.AddEnergy(energy);
    }
}
