using System;
using System.Collections.Generic;
using UnityEngine;

public class HumanSpawner : MonoBehaviour
{
    public List<GameObject> SpawnPoints;
    [SerializeField] private GameObject _humanPrefab;

    private void Start()
    {
        foreach (var spawnPoint in SpawnPoints)
        {
            var human = Instantiate(_humanPrefab);
            human.transform.SetParent(spawnPoint.transform);
            human.transform.localPosition = Vector3.zero;
            human.transform.localRotation = Quaternion.identity;
          //  human.transform.localScale = Vector3.one;
        }
    }
}
