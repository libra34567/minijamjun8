﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanHealth : Health
{
    public override void ApplyDamage(float damage)
    {
        if (health - damage <= 0)
        {
            GameObject.Find("ScoreController").GetComponent<ScoreController>().UpdatePopulation(1);
        }

        base.ApplyDamage(damage);
        
    }
}
